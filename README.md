# memint
Memint (**mem**orize **int**egers) is a GUI program to help with memorization of ... integers!  Decimal numbers are allowed, but the decimal point will not be recorded, and will be ignored if typed.

## Features

* Can give random numbers, pi, *e*, or user-defined.
* Make a certain number of digits cement into your head, or add more digits as you get them right (at your own pace).
* Digits that you get wrong are marked in red.  New digits (ones you haven't memorized) are marked in green.

## Screenshots

![Screenshot 1](https://cloud.githubusercontent.com/assets/9973450/20310171/d68eda44-ab42-11e6-9fca-dfbd86482734.png)
![Screenshot 2](https://cloud.githubusercontent.com/assets/9973450/20310269/3436f4f6-ab43-11e6-9638-b32803201d24.png)

With the help of this program, I have memorized 185 digits of pi. (As of Nov. 15, 2016)
