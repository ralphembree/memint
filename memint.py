import math
import random

import gtk
import pango

class Window(gtk.Window):
    TEST = "test"
    SHOW = "show"
    NEW_COLOR = "green"
    BAD_COLOR = "red"
    def __init__(self):
        self.__class__.__base__.__init__(self)
        self.set_title("Memint")
        self.set_position(gtk.WIN_POS_CENTER)
        self.connect("destroy", gtk.main_quit)
        self.connect("window-state-event", self.on_fullscreen_event)
        self.connect("key-press-event", self.on_key)
        self.hbox = gtk.HBox()
        self.add(self.hbox)
        self.left_vbox = gtk.VBox()
        self.right_vbox = gtk.VBox()
        self.label = gtk.Label()
        self.label.set_selectable(True)
        self.label_button = gtk.Button()
        self.label_button.show()
        self.label_ebox = gtk.EventBox()
        self.label_ebox.add(self.label)
        self.hbox.pack_start(self.left_vbox, False)
        self.hbox.pack_start(self.label_ebox)
        self.hbox.pack_end(self.right_vbox, False)
        self.random_button = gtk.CheckButton("_Random")
        self.random_button.set_active(True)
        self.random_button.connect("toggled", self.on_random_button_toggled)
        self.left_vbox.pack_start(self.random_button, False)
        self.spinbuttons = [[gtk.Label("_Number of digits:"), gtk.SpinButton(gtk.Adjustment(1, 1, 200, 1, 5, 0), 1, 0)], [gtk.Label("_Climb rate:"), gtk.SpinButton(gtk.Adjustment(1, 1, 50, 1, 3, 0), 1, 0)]]
        self.spinbuttons[0][0].set_mnemonic_widget(self.spinbuttons[0][1])
        self.spinbuttons[1][0].set_mnemonic_widget(self.spinbuttons[1][1])
        self.spinbuttons[0][0].set_use_underline(True)
        self.spinbuttons[1][0].set_use_underline(True)
        self.spinbuttons[0][1].set_value(1)
        self.spinbuttons[0][1].connect("notify::value", self.on_place_changed)
        self.left_vbox.pack_start(self.spinbuttons[0][0], False)
        self.left_vbox.pack_start(self.spinbuttons[0][1], False)
        self.left_vbox.pack_start(self.spinbuttons[1][0], False)
        self.spinbutton_alignment = gtk.Alignment(0, 0)
        self.spinbutton_alignment.add(self.spinbuttons[1][1])
        self.left_vbox.pack_start(self.spinbutton_alignment, False)
        self.spinbuttons[1][0].set_alignment(0, 1)
        self.pause_button = gtk.CheckButton("_Manual")
        self.left_vbox.pack_start(self.pause_button, False)
        self.pause_button.connect("toggled", self.on_pause_button_toggled)
        self.pause_button.set_active(False)
        self.set_button = gtk.Button("_Set")
        self.set_button.connect("clicked", self.on_set_button_clicked)
        self.left_vbox.pack_end(self.set_button, False)
        self.ok_button = gtk.Button("_OK")
        self.ok_button.set_flags(gtk.CAN_DEFAULT)
        self.ok_button.connect("clicked", self.switch_mode)
        self.right_vbox.pack_end(self.ok_button, False)
        self.ok_button.grab_default()
        self.random = True
        self.random_number = "".join(str(self.get_rand()) for i in range(4))
        self.user_number = ""
        self.mode = self.SHOW
        self.is_fullscreen = False
        self.set_place(4)
        self.set_markup()

    def on_pause_button_toggled(self, button):
        self.spinbuttons[1][0].set_sensitive(not button.get_active())
        self.spinbuttons[1][1].set_sensitive(not button.get_active())

    def on_random_button_toggled(self, button):
        self.random = button.get_active()
        place = self.get_place()
        self.set_place(place)
        if not self.random:
            def on_changed(textbuffer, id):
                textbuffer.handler_block(id[0])
                text = textbuffer.get_text(textbuffer.get_start_iter(), textbuffer.get_end_iter(), False)
                new_text = ""
                for char in text:
                    if char in "0123456789":
                        new_text += char
                textbuffer.set_text(new_text)
                textbuffer.handler_unblock(id[0])
            dialog = gtk.Dialog("What number?", self, gtk.DIALOG_MODAL,
                               ("_\xcf\x80", 0,
                                "", 1,
                                gtk.STOCK_OK, gtk.RESPONSE_OK)
            )
            accelgroup = gtk.AccelGroup()
            dialog.add_accel_group(accelgroup)
            pi = dialog.get_action_area().get_children()[2]
            pi.add_accelerator("activate", accelgroup, ord("p"), gtk.gdk.MOD1_MASK, gtk.ACCEL_MASK)
            dialog.get_action_area().get_children()[1].child.set_markup_with_mnemonic("<i>_e</i>")
            textview = gtk.TextView()
            textbuffer = textview.get_buffer()
            textbuffer.set_text(self.user_number)
            textview.show()
            id = []
            id.append(textview.get_buffer().connect("changed", on_changed, id))
            dialog.vbox.pack_start(textview)
            textview.set_wrap_mode(gtk.WRAP_CHAR)
            response = dialog.run()
            if response == 0:
                self.user_number = open("/home/commandant/r/misc/pi1000").read().strip().replace(".", "")
            elif response == 1:
                self.user_number = open("/home/commandant/r/misc/e1000").read().strip().replace(".", "")
            elif response == gtk.RESPONSE_OK:
                self.user_number = textview.get_buffer().get_text(textbuffer.get_start_iter(), textbuffer.get_end_iter(), False)
            dialog.destroy()
            if not self.user_number:
                self.set_place(place)
                self.random_button.set_active(True)
                return
            self.spinbuttons[0][1].get_adjustment().set_upper(len(self.user_number))
        self.set_markup()

    def on_set_button_clicked(self, widget):
        if self.mode == self.TEST: self.switch_mode()
        if self.random:
            self.random_number = ""
            self._set_place(self.get_place())
        else:
            self.random_button.emit("toggled")

    def on_key(self, widget, event):
        if (event.keyval == gtk.keysyms.F11) or ((event.keyval == gtk.keysyms.Escape) and self.is_fullscreen):
            self.toggle_fullscreen()
            return

    def on_label_key(self, widget, event):
        if ord("0") <= event.keyval <= ord("9"): 
            self.user_string += chr(event.keyval)
        elif gtk.keysyms.KP_0 <= event.keyval <= gtk.keysyms.KP_9:
            self.user_string += str(event.keyval - gtk.keysyms.KP_0)
        elif event.keyval == gtk.keysyms.BackSpace:
            self.user_string = self.user_string[:-1]
        elif event.keyval in (gtk.keysyms.Return, gtk.keysyms.KP_Enter):
            self.ok_button.activate()
        else: return
        if len(self.user_string) > self.get_place():
            self.set_place(len(self.user_string))
        self.set_markup(self.user_string)

    def on_place_changed(self, *args):
        place = self.get_place()
        if self.random and place >= len(self.random_number):
            for i in range(place - len(self.random_number)):
                self.random_number += str(self.get_rand())
        self.set_markup()
        return False

    def on_fullscreen_event(self, widget, event):
        if event.changed_mask & gtk.gdk.WINDOW_STATE_FULLSCREEN:
            self.is_fullscreen = event.new_window_state & gtk.gdk.WINDOW_STATE_FULLSCREEN

    def toggle_fullscreen(self, *args):
        if self.is_fullscreen:
            self.unfullscreen()
        else:
            self.fullscreen()

    def switch_mode(self, *args):
        if self.mode == self.SHOW:
            self.mode = self.TEST
            self.set_markup("")
            self.user_string = ""
            self.onkey_signal = self.label_ebox.connect("key-press-event", self.on_label_key)
            self.label.reparent(self.label_button)
            self.label_ebox.add(self.label_button)
            self.label_button.grab_focus()
        elif self.mode == self.TEST:
            self.mode = self.SHOW
            self.label_ebox.remove(self.label_button)
            self.label.reparent(self.label_ebox)
            self.label_ebox.disconnect(self.onkey_signal)
            self.set_markup(user_text=self.label.get_text().replace(" ", "").replace("\n", ""))
            

    def set_markup(self, text=None, user_text=None):
        root = gtk.gdk.get_default_root_window()
        root_x, root_y = root.get_size()
        # Multiplied by 2.6 to account for letters being taller than their width
        dec = (root_x / float(root_y)) * 2.6
        place = self.get_place()

        if user_text is None:
            if text is None:
                text = self.get_number()[:self.get_place()]
            length = len(text)
            num_chars = max(length, place)
            y = math.ceil((num_chars / dec) ** 0.5)
            x = math.ceil(num_chars / y)
            new_text = []
            line_pos = 0
            for i in range(num_chars):
                new_text.append("<u>%c</u>" % (text[i] if i < length else " "))
                line_pos += 1
                if line_pos == x:
                    new_text.append("\n")
                    line_pos = 0

            new_text = " ".join(new_text)
        else:
            user_text = user_text.strip()
            text = self.get_number()
            changed = False
            length = len(user_text)
            num_chars = max(length, place)
            y = math.ceil((num_chars / dec) ** 0.5)
            x = math.ceil(num_chars / y)
            line_pos = 0
            chars = []
            for i in range(num_chars):
                cur = "<u>{}</u>"
                if i >= length or user_text[i] != text[i]:
                    changed = True
                    if i >= length:
                        color = self.NEW_COLOR
                    else:
                        color = self.BAD_COLOR
                    cur = cur.format('<span color="%s">%c</span>' % (color, text[i]))
                else:
                    cur = cur.format(user_text[i])

                line_pos += 1
                if line_pos == x:
                    cur += "\n"
                    line_pos = 0
                else:
                    cur += " "
                chars.append(cur)
            new_text = "".join(chars)
            if not changed:
                for char in self.get_next():
                    new_text += '<span underline="single" color="%s">%c</span> ' % (self.NEW_COLOR, char)
        text = new_text.strip()
        self.label.set_markup(text)

    def set_place(self, number):
        self.spinbuttons[0][1].get_adjustment().set_value(float(number))
        self._set_place(number)

    def get_place(self):
        return int(self.spinbuttons[0][1].get_adjustment().get_value())

    def _set_place(self, number):
        while number >= len(self.random_number):
            n = int(round(random.random(), 1) * 10)
            if n == 10: n = 1
            self.random_number += str(n)
        self.set_markup(self.random_number[:number])

    def get_rand(self):
        number = int(round(random.random(), 1) * 10)
        if number == 10: return 1
        else: return number

    def get_next(self):
        if self.pause_button.get_active(): return ""
        place = self.get_place()
        change = self.spinbuttons[1][1].get_value_as_int()
        if place + change >= len(self.get_number()):
            if not self.random:
                self.set_place(len(self.user_number))
                return self.user_number[place:]
            else:
                string = ""
                for char in self.random_number[place:]:
                    string += char
                    change -= 1
                for i in range(change):
                    string += str(self.get_rand())
                    self.random_number += string[-1]
                self.set_place(len(self.random_number))
                return string
        else:
            self.set_place(place + change)
            return self.get_number()[place:place + change]

    def get_number(self):
        if self.random: return self.random_number
        else: return self.user_number

if __name__ == "__main__":
    window = Window()
    window.show_all()
    gtk.mainloop()
